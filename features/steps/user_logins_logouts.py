from selenium import webdriver
from behave import Given, When, Then
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

@Given('user proceeds to login page')
def step_impl(context):
    context.browser.get('https://console.awan.sh/')
    context.browser.refresh()
    WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/img')))

@When('user enters unregistered username')
def step_impl(context):
    WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div[2]/div/div[2]/input'))).send_keys('aldebaran123')
    #context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div[2]/div/div[2]/input').send_keys('aldebaran123')

@When('user enters unregistered password')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]/div/div[2]/input').send_keys('awanio123')

#@When('user clicks show or hide password')
#def step_impl(context):
    #context.browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    #WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]/div/div[3]/div/div/div'))).click()
#    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]/div/div[3]/div/div/div').click()

@When('user clicks login button')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div').click()
    time.sleep(8)
#@When('error message is shown')
#def step_impl(context):
#    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '///*[@id="root"]/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div'))).click()
# 
# if context.browser.find_element(By.XPATH, '///*[@id="root"]/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div'):
#      print('element exists')
#   else:
#       return False

@When('user enters registered username')
def step_impl(context):
    username_element = context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div[2]/div/div[2]/input')
    #WebDriverWait(context.browser, 3).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div[2]/div/div[2]/input'))).clear().send_keys('aldebaran')
    username_element.send_keys(Keys.CONTROL + "a")
    username_element.send_keys(Keys.DELETE)
    username_element.send_keys('aldebaran')
    time.sleep(5)

@When('user enters registered password')
def step_impl(context):
    #context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]/div/div[2]/input').clear().send_keys('awanio')
    password_element = context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]/div/div[2]/input')
    #WebDriverWait(context.browser, 3).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/div[2]/div/div[2]/input'))).clear().send_keys('aldebaran')
    password_element.send_keys(Keys.CONTROL + "a")
    password_element.send_keys(Keys.DELETE)
    password_element.send_keys('awanio')
    time.sleep(5)

@When('user clicks login button for the second attempt')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div/div/div').click()
    time.sleep(5)

@Then('user proceeds to the dashboard page')
def step_impl(context):
     context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div[1]/div/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]/div/div/div/div[1]/div[2]/div[2]/div[1]')
    #try:
    #    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div[1]/div/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]/div/div/div/div[1]/div[2]/div[2]/div[1]')
    #except NoSuchElementException:
    #    return False
    #return True

@When('user clicks profile')
def step_impl(context):
    WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div[1]/div/div/div/div/div[1]/div/div/div[2]/div/div/div[1]/div/div/div/div[2]/div'))).click()
    #context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div[1]/div/div/div/div/div[1]/div/div/div[2]/div/div/div[1]/div/div/div/div[2]/div').click()
    time.sleep(5)

@When('user selects logout')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div[5]/div[2]/div/div/div[4]/div').click()
    time.sleep(5)

@When('user selects yes')
def step_impl(context):
    WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[1]/img'))).click()

@Then('user goes back to login page')
def step_impl(context):
    time.sleep(5)
    try:
        WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[2]/div/div[2]/div/div[1]/div[2]/div/div/div[1]')))
    except NoSuchElementException:
        return False
    return True
