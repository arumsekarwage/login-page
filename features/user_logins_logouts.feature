Feature: login page

    Scenario: user logins with unregistered and registered data, tries to logouts, and forgets password
        Given user proceeds to login page
        When user enters unregistered username
        And user enters unregistered password
    #    And user clicks show or hide password
        And user clicks login button
    #    When error message is shown
        When user enters registered username
        When user enters registered password
        When user clicks login button for the second attempt
        Then user proceeds to the dashboard page
        When user clicks profile
        And user selects logout
        And user selects yes
        Then user goes back to login page